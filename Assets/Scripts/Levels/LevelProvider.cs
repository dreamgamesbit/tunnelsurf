﻿using System;
using Configuration.Levels;
using Levels.Creation;
using Levels.ConfigProvider;
using UnityEngine;

namespace Levels.LevelProvider
{
    public class LevelProvider
    {
        private readonly LevelCreator _levelCreator = default;
        private readonly LevelConfigProvider _levelConfigProvider = default;
        private Level _currentLevel = default;
        private LevelConfig _currentLevelConfig = default;

        public Level CurrentLevel => _currentLevel;

        public LevelProvider(LevelCreator levelCreator,
        LevelConfigProvider levelConfigProvider
        // Add argument for tunnel generation
        )
        {
            _levelCreator = levelCreator;
            _levelConfigProvider = levelConfigProvider;
        }

        public Level LoadLevel(int levelNumber)
        {
            try
            {
                var levelConfig = _levelConfigProvider.LoadConfig(levelNumber);
                // generate tunnel and provide to level
                var level = new Level(levelConfig);
                _currentLevel = level;
                _currentLevelConfig = levelConfig;

                return level;
            }
            catch (InvalidOperationException exception)
            {
                Debug.LogError("Cannot load level beacuse of error: " + exception.ToString());
                return null;
            }
            catch (Exception exception)
            {
                Debug.LogError("Cannot load level beacuse of unexpected error: " + exception.ToString());
                return null;
            }
        }

        public void UnloadCurrentLevel() 
        {
            if (_currentLevelConfig != null)
            {
                _levelConfigProvider.UnloadConfig(_currentLevelConfig);
            }
            _currentLevel?.Dispose();
            
            _currentLevelConfig = null;
            _currentLevel = null;
        }
    }
}
