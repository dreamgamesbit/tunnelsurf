﻿using System;
using Configuration.Levels;

namespace Levels 
{
    public class Level : IDisposable
    {
        private int _levelNumber = default;
        private float _currentProgress = default;
        private LevelConfig _config = default;
        public int LevelNumber => _levelNumber;

        //TODO:
        // - add field for generated tunnel
        // - add tunnel argument in ctor
        public Level(LevelConfig config) 
        {
            _levelNumber = config.LevelNumber;
            //TODO:
            // - Destroy(tunnel);
            _config = config;
        }

        public void Dispose()
        {
            _config = null;
        }
    }
}

