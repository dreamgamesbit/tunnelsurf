﻿using System;
using Configuration.Levels;

namespace Levels.Creation
{
    public class LevelCreator
    {
        // Maybe make it ctor?
        public void Initialize()
        {
            // If some initialization needed
        }

        // Add argument for generated tunnel
        public Level CreateLevel(LevelConfig levelConfig) 
        {
            if (levelConfig == null)
                throw new ArgumentNullException("Provided level config cannot be null.");

            var result = new Level(levelConfig);

            return result;
        }

        public void UpdateLevel(Level level)
        {
            // ?
        }
    }
}
