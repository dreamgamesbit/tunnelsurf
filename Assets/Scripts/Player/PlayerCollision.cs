﻿using System;
using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(BoxCollider))]
    public class PlayerCollision : MonoBehaviour
    {
        private const string _obstacleTag = "Obstacle";
        private const string _steepTag = "Steep";
        private const string _finishLineTag = "FinishLine";

        public event Action OnCollisionWithObstacle = () => {};
        public event Action OnDropInSteep = () => {};
        public event Action OnCrossingFinishLine = () => {};

        private void OnCollisionEnter(Collision other) 
        {
            if (other.gameObject.CompareTag(_obstacleTag)){OnCollisionWithObstacle?.Invoke();}
            if (other.gameObject.CompareTag(_steepTag)){OnDropInSteep?.Invoke();}
            if (other.gameObject.CompareTag(_finishLineTag)){OnCrossingFinishLine?.Invoke();}
        }
    }   
}