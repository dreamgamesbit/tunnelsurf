﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralGeometry : MonoBehaviour
{
    
    void Start()
    {
        Mesh mesh = new Mesh();

        mesh.name = "NewQuad";

        List<Vector3> points = new List<Vector3>()
        {
            new Vector3(-1, -1),
            new Vector3(1, -1),
            new Vector3(-1, 1),
            new Vector3(1, 1),
        };

        int[] triangleIndex = new int[]
        {
            2,0, 1,
            2, 1, 3
        };
        
        List<Vector3> normals = new List<Vector3>()
        {
            new Vector3(0,0,-1),
            new Vector3(0,0,-1),
            new Vector3(0,0,-1),
            new Vector3(0,0,-1)
        };
        
        mesh.SetVertices(points);
        mesh.triangles = triangleIndex;
        mesh.SetNormals(normals);
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

}
