﻿using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
    public Collider _collider = default;

    // This method exists for specific obstacles
    // (for bombs with timer for exmaple).
    // This will be refactored in feature.
    public abstract void StartAction();
    public Collider Collider => _collider;
}
