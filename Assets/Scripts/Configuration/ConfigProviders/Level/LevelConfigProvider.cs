﻿using System;
using System.IO;
using Configuration.Levels;
using UnityEngine;

namespace Levels.ConfigProvider
{
    public class LevelConfigProvider
    {
        private const string _configsPath = "Assets/Resources/LevelConfigs";

        public LevelConfig LoadConfig(int number)
        {
            var configPath = Path.Combine(_configsPath, number.ToString());
            var config = Resources.Load<LevelConfig>(configPath);
            if (config == null) 
            {
                throw new InvalidOperationException($"Cannot load config at path {configPath} from resources. Check if it exists.");
            }

            return config;
        }

        public void UnloadConfig(LevelConfig config)
        {
            if (config == null) 
            {
                Debug.LogError("Provided config is null");
                return;
            }
            Resources.UnloadAsset(config);
        }
    }
}
