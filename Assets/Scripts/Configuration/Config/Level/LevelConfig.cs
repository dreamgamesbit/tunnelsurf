﻿using System;
using UnityEngine;

namespace Configuration.Levels
{

    [Serializable]
    public class ObstacleLayoutContainer 
    {
        public Obstacle target = default;
        public float minLayoutRange = 30.0f;
        public float maxLayoutRange = 120.0f;
        public float generationPercentage = 20.0f;
    }

    [CreateAssetMenu(menuName = "Configuration/Levels/LevelConfig")]
    public class LevelConfig : ScriptableObject
    {
        [SerializeField]
        private int _levelNumber = 0;
        [SerializeField]
        private int _levelLength = 500;
        [SerializeField]
        private int _obstaclesCount = 5;
        [SerializeField]
        private ObstacleLayoutContainer [] _allowableObstacles = new ObstacleLayoutContainer [1];

        public int LevelNumber => _levelNumber;
        public int LevelLength => _levelLength;
        public int ObstaclesCount => _obstaclesCount;
        public ObstacleLayoutContainer [] AllowableObstacles => _allowableObstacles;

        private void OnValidate() 
        {
            if (_levelLength == 0) Debug.LogError("Level length equals zero.");
            if (_obstaclesCount == 0) Debug.LogError("Obstacles count equals zero. Please provide at least one obstacle");
            if (_allowableObstacles.Length == 0) Debug.LogError("Allowable obstacles is empty. Please provide at least one obstacle.");
        }
    }

}
